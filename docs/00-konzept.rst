Satzungsdokumente der L13 FoodCoop Freiburg
===========================================

Die vorliegende Satzung, Vereinsordnung und Schlichtungsordnung zeigen modellhaft, wie sich
ein Verein auf den Grundlagen von Konsent, Soziokratie und Holakratie strukturieren kann.

Aber niemals vergessen: Die besten Regeln helfen nichts, wenn es an der Reife des Vereins und seiner Mitglieder mangelt. Selbstorganisation ist Freiheit in Verantwortlichkeit. Und Freiheit in Verantwortlichkeit ist eine große Übung. Für jede einzelne Person, für lokale Gemeinschaften, Staaten und die gesamte Menschheit.

Das gesagt, sind alle Materialien ge-copylefted unter der `Creative Commons CC-BY-SA 4.0 DE Lizenz https://creativecommons.org/licenses/by-sa/4.0/deed.de` frei verfügbar. Um die Bedingungen der Lizenz zu erfüllen, reicht ein Zusatz der Form:

> Abgeleitet von der docs.L13.org Satzung und frei unter der CC-BY-SA 4.0 DE Lizenz verfügbar.

Für Geeks: Du kannst dazu einfach gitlab.com/l13/docs forken und mit readthedocs.org verbinden.

Have Fun! Die L13 Konsent Gruppe

TODOs
-----

Was bei einer der kommenden Überarbeitungen getan werden könnte:

* Satzung und Vereinsordnung zusammenführen.
  Die Vereinsordnung wurde einst ausgegliedert, um Detailregelungen auch ohne Registereintrag
  anpassen zu können. Mit der Einführung der halbjährlichen Kerngruppenwahl muss ohnehin ein
  Registereintrag erfolgen, so dass eine Zusammenführung größere Klarheit bieten kann.
* Basis-Satzung verbessern.
  Die Verlinkte *Holacracy Konstitution* von Cidpartners ist (ebenso wie die Variante von
  Drarfs&Giants) nahe am Original und enthält viele Anglizismen. Einen kulturelle Übersetzung
  verspricht, die Verständlichkeit zu verbessern.

